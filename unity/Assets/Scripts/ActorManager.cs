﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ActorManager : MonoBehaviour
{
    [SerializeField]
    private GameObject _actors;

    // Dictionary of actor names to actors
    private Dictionary< string, Actor > _actorDict = new Dictionary<string, Actor>();

    // Special reference to Grandpa's sprite renderer
    [SerializeField]
    SpriteRenderer _grandpaSpriteRenderer;

    // Whether or not grandpa is nude
    private bool _grandpaNude = false;
    public bool GrandpaNude { get { return _grandpaNude; } }

	// Use this for initialization
	void Start()
	{
        // Initialize the actor dictionary
        foreach( Actor actor in _actors.GetComponentsInChildren<Actor>() )
        {
            _actorDict.Add( actor.name, actor );
        }
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}
    
    public Actor GetActor( string name )
    {
        return _actorDict[ name ];
    }

    public void OnOutfitChanged( string newOutfit )
    {
        _grandpaSpriteRenderer.sprite = Resources.Load( "Images/Characters/Grandpa/" + newOutfit, typeof( Sprite ) ) as Sprite;

        // Set the grandpa nude flag
        _grandpaNude = ( newOutfit == "grandpa_naked" );
    }
}
