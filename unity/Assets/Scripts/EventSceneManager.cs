﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Xml.Serialization;
using System.IO;

public class EventSceneManager : MonoBehaviour
{
    [SerializeField]
    private SpriteRenderer _backgroundSpriteRenderer;
    [SerializeField]
    private Image _finalImage;
    [SerializeField]
    private StatCheck _statCheckAnimation;
    [SerializeField]
    private Animator _addStrikeAnimation;

    [Header("Actors")]
    [SerializeField]
    private GameObject _leftActorName;
    [SerializeField]
    private Text _leftActorNameTextBox;
    
    [SerializeField]
    private GameObject _rightActorName;
    [SerializeField]
    private Text _rightActorNameTextBox;

    private Actor _leftActor;
    private Actor _rightActor;
    private ActorManager _actorManager;
    private Actor _currentActor;

    [Header("Buttons")]
    [SerializeField]
    private GameObject _nextButton;

    [Header("Text")]
    [SerializeField]
    private Text _eventTextBox;
    Script _script;
    private string _scriptFileName;

    // Member variables to track text state
    private Message _currentMessage;
    private int _currentMessageIndex;
    private int _currentMessageTextIndex;
    private string _currentMessageProgress;
    private bool _printingMessage = false;
    private bool _playStatCheckAnimation = false;
    private bool _waitingOnStatCheckAnimation = false;
    private bool _waitingForActorTeardown = false;
    private bool _sceneFinished = false;
    private bool _lastStatCheckPassed = false;

    // GameInfo variables
    private GameInfo _gameInfo;
    private SceneLoader _sceneLoader;
    private StatManager _statManager;

    // Audio member variables
    private AudioSource _musicAudioSource;
    private AudioSource _voiceAudioSource;
    private int _audioFrameCounter;
    
    // Use this for initialization
    void Start()
	{
        GameObject gameInfo = GameObject.Find( "GameInfo" );
        _gameInfo = gameInfo.GetComponent<GameInfo>();
        _sceneLoader = gameInfo.GetComponent<SceneLoader>();
        _actorManager = gameInfo.GetComponent<ActorManager>();
        _statManager = gameInfo.GetComponent<StatManager>();
        _voiceAudioSource = GetComponent<AudioSource>();
        _musicAudioSource = GameObject.Find( "MusicPlayer" ).GetComponent<AudioSource>();
        
        // Hide some UI elements
        _nextButton.SetActive( false );
        _leftActorName.SetActive( false );
        _rightActorName.SetActive( false );

        // Parse the script from the UI Manager
        ParseScript();

        // Start printing the first message
        StartNextMessage();
	}
	
	// Update is called once per frame
	void Update()
	{
	    if( _printingMessage )
        {
            // If this is the first character of the message and there is a sound clip to play, play it
            if( ( null != _currentMessage.SoundClip ) && ( 0 == _currentMessageTextIndex ) )
            {
                // Just use the music source to play the sound clip
                AudioClip soundClip = Resources.Load( "Sounds/" + _currentMessage.SoundClip, typeof( AudioClip ) ) as AudioClip;
                _musicAudioSource.PlayOneShot( soundClip );
            }

            // If input is pressed, immediately finish the text
            if( Input.anyKeyDown )
            {
                _eventTextBox.text = _currentMessage.Text;
                _printingMessage = false;
            }
            else
            {
                if( _currentMessageTextIndex < _currentMessage.Text.Length )
                {
                    // Add a character to the message each frame
                    char nextCharacter = _currentMessage.Text[ _currentMessageTextIndex++ ];
                    _currentMessageProgress += nextCharacter;
                    _eventTextBox.text = _currentMessageProgress;

                    // Don't play voice clips on spaces
                    if( ' ' != nextCharacter )
                    {
                        // Only play a voice sound every few frames so the audio clips don't overlap
                        if( 0 == _audioFrameCounter )
                        {
                            _voiceAudioSource.pitch = UnityEngine.Random.Range( _currentActor.VoiceHighPitch, _currentActor.VoiceLowPitch );
                            _voiceAudioSource.PlayOneShot( _currentActor.Voice, _currentActor.Volume );
                        }

                        // Increase the audio frame counter
                        _audioFrameCounter = ( ++_audioFrameCounter % _currentActor.AudioFrameDelay );
                    }
                }
                else // no more characters to add
                {
                    _printingMessage = false;
                }
            }
        }
        else if( Input.anyKeyDown ) // If we aren't printing a message, call the OnMessageComplete callback
        {
            // Transition the actor out if we need to.  If there is no actor to transition, just call OnMessageComplete()
            // Otherwise, the transition's callback completion will trigger the next message.
            if( !_sceneFinished && !_waitingForActorTeardown && !_waitingOnStatCheckAnimation && !TeardownActors() )
            {
                OnMessageComplete();
            }
        }
    }

    void ParseScript()
    {
        _scriptFileName = _sceneLoader.GetCurrentXMLFile();
        TextAsset scriptFile = Resources.Load( "EventScripts/" + _scriptFileName, typeof( TextAsset ) ) as TextAsset;
        XmlSerializer serializer = new XmlSerializer( typeof( Script ) );
        using( StringReader reader = new StringReader( scriptFile.text ) )
        {
            _script = (Script) serializer.Deserialize( reader );
        }
        
        // Set up the first message
        _currentMessageIndex = 0;
        _currentMessage = _script.Messages[ _currentMessageIndex ];
    }

    void StartNextMessage()
    {
        // The message may already be printing when this is called (e.g. called as part of a coroutine)
        if( !_sceneFinished && !_printingMessage && !_waitingOnStatCheckAnimation )
        {
            // Reset the text
            _eventTextBox.text = "";

            // Play music if there is any
            PlayMusic();

            // Change the background scene if needed
            SetBackgroundSprite();

            // Get the current actor
            _currentActor = _actorManager.GetActor( _currentMessage.Actor.Name );

            // Setup the actors and the text box
            SetupActors();

            // If this message is a branching message, play the stat check animation based on the check results from the last message
            // and the flag from the xml file.  When the animation completes, then start the actual dialogue.
            if( null != _currentMessage.BranchName && _playStatCheckAnimation )
            {
                _statCheckAnimation.GetComponent<Animator>().Play( _lastStatCheckPassed ? "StatCheck_PASS" : "StatCheck_FAIL" );
                _waitingOnStatCheckAnimation = true;
                _playStatCheckAnimation = false;
            }
            else // not a branch, start dialogue right away
            {
                _printingMessage = true;
            }

            AddStrike();

            _currentMessageProgress = "";
            _currentMessageTextIndex = 0;
            _audioFrameCounter = 0;
        }
    }

    void OnMessageComplete()
    {
        // No more messages to process
        if( _currentMessage.LastMessage )
        {
            // If this is the final image (for a win/gameover screen), draw the final image over top of the scene and enable it
            if( null != _currentMessage.FinalImage )
            {
                _finalImage.sprite = Resources.Load( "Images/Backgrounds/" + _currentMessage.FinalImage, typeof( Sprite ) ) as Sprite;
                _finalImage.gameObject.SetActive( true );
            }
            else // Otherwise just activate the next button
            {
                _nextButton.SetActive( true );
            }

            _sceneFinished = true;
        }
        else // more messages to process
        {
            // No stat check, just go to the next message
            MessageStatCheck statCheck = _currentMessage.StatCheck;
            if( null == statCheck )
            {
                _currentMessage = _script.Messages[ ++_currentMessageIndex ];
            }
            else // there is a stat check, find the message that we need to jump to
            {
                // Set the flag for the next message to play the animation
                _lastStatCheckPassed = ( _statManager.GetStatValue( statCheck.Stat ) >= statCheck.PassValue );

                // Set the branch to jump to based on whether we passed or failed the stat check
                string branchToJumpTo = ( _lastStatCheckPassed ? statCheck.PassBranch : statCheck.FailBranch );
                
                // Set the next message
                _currentMessageIndex = Array.FindIndex( _script.Messages, element => ( ( null != element.BranchName ) && ( branchToJumpTo == element.BranchName ) ) );
                _currentMessage = _script.Messages[ _currentMessageIndex ];

                // Set the animation's stat text and callback for the next message if we are playing it
                if( statCheck.PlayAnimation )
                {
                    _statCheckAnimation.StatText.text = statCheck.Stat;
                    _statCheckAnimation.setStatCheckFinishedCallback( OnStatCheckAnimationFinished );
                    _playStatCheckAnimation = true;
                }
            }
        }

        // Start the next message
        StartNextMessage();
    }

    void AddStrike()
    {
        // Add the strike if this mesasge needs to
        if( _currentMessage.AddStrikes > 0 )
        {
            _addStrikeAnimation.GetComponent<Animator>().Play( "STRIKE" );
            _gameInfo.AddStrikes( _currentMessage.AddStrikes );
        }
    }

    void PlayMusic()
    {
        if( null != _currentMessage.Music )
        {
            // Set the music to the current clip on the audio source and then play it
            _musicAudioSource.clip = Resources.Load( "Music/" + _currentMessage.Music, typeof( AudioClip ) ) as AudioClip;
            _musicAudioSource.Play();
        }
    }

    void SetBackgroundSprite()
    {
        if( null != _currentMessage.BackgroundSprite )
        {
            // Set the background image
            _backgroundSpriteRenderer.sprite = Resources.Load( "Images/Backgrounds/" + _currentMessage.BackgroundSprite, typeof( Sprite ) ) as Sprite;
        }
    }

   void SetupActors()
    {
        // Transition the actor in if we need to and set the name
        if( "Left" == _currentMessage.Actor.Side )
        {
            // Create the actor and transition xir in
            if( ( null != _currentMessage.Transition ) && _currentMessage.Transition.In )
            {
                _leftActor = Instantiate( _currentActor, new Vector3( -3.0f, 0.0f, 0.0f ), Quaternion.identity ) as Actor;
                _leftActor.GetComponent<Animator>().Play( "TransitionIn" );
            }

            // Show/hide the appropriate text boxes
            _leftActorName.SetActive( true );
            _rightActorName.SetActive( false );

            // Set the textbox name
            _leftActorNameTextBox.text = _currentActor.name;
        }
        else if( "Right" == _currentMessage.Actor.Side )
        {
            // Create the actor and transition xir in
            if( ( null != _currentMessage.Transition ) && _currentMessage.Transition.In )
            {
                _rightActor = Instantiate( _currentActor, new Vector3( 3.0f, 0.0f, 0.0f ), Quaternion.identity ) as Actor;
                _rightActor.GetComponent<Animator>().Play( "TransitionIn" );
            }

            // Show/hide the appropriate text boxes
            _leftActorName.SetActive( false );
            _rightActorName.SetActive( true );

            // Set the textbox name
            _rightActorNameTextBox.text = _currentActor.name;
        }
        else // if( "Narrator" == _currentMessage.Actor.Side )
        {
            // Hide all name text boxes
            _leftActorName.SetActive( false );
            _rightActorName.SetActive( false );

            _leftActorNameTextBox.text = "";
            _rightActorNameTextBox.text = "";
        }
    }

    bool TeardownActors()
    {
        bool ret = false;
        if( ( null != _currentMessage.Transition ) && _currentMessage.Transition.Out )
        {
            if( "Left" == _currentMessage.Actor.Side )
            {
                if( null != _leftActor )
                {
                    // Play the "transition out" animation.  Actor will be destroyed on completion.
                    _leftActor.GetComponent<Animator>().Play( "TransitionOut" );
                    _leftActor.setTransitionOutCallback( OnTransitionOutFinished );
                    _leftActor = null;

                    _waitingForActorTeardown = true;
                    ret = true;
                }
            }
            else if( "Right" == _currentMessage.Actor.Side )
            {
                if( null != _rightActor )
                {
                    // Play the "transition out" animation.  Actor will be destroyed on completion.
                    _rightActor.GetComponent<Animator>().Play( "TransitionOut" );
                    _rightActor.setTransitionOutCallback( OnTransitionOutFinished );
                    _rightActor = null;

                    _waitingForActorTeardown = true;
                    ret = true;
                }
            }
            // else if( "Narrator" == _currentMessage.Actor.Side ) do nothing
        }

        return ret;
    }

    public void OnTransitionOutFinished( Actor actor )
    {
        // If an actor has transitioned out completely, they can be destroyed
        Destroy( actor.gameObject );

        // Reset the flag and call the OnMessageComplete callback to determine the next message to start
        _waitingForActorTeardown = false;
        OnMessageComplete();
    }

    public void OnStatCheckAnimationFinished( StatCheck statCheck )
    {
        // Once the animation is complete, start the dialogue
        _waitingOnStatCheckAnimation = false;
        _printingMessage = true;
    }
    
    public void OnEventComplete()
    {
        // Destroy any actors still alive
        if( null != _leftActor )
        {
            Destroy( _leftActor.gameObject );
        }

        if( null != _rightActor )
        {
            Destroy( _rightActor.gameObject );
        }

        // If we got to week 12 without striking out, set the nope test flag
        if( 12 == _gameInfo.Week )
        {
            _gameInfo.NopeTestDone = true;
        }

        if( _scriptFileName.Contains( "Event" ) )
        {
            _sceneLoader.OnSceneEnded( SceneLoader.SceneEnum.Event );
        }
        else // Special case for game over cases (strikeouts, wins)
        {
            _sceneLoader.OnSceneEnded( SceneLoader.SceneEnum.Gameover );
        }
    }
}
