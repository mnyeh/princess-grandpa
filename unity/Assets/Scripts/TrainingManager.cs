﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TrainingManager : MonoBehaviour
{
    private StatManager _statManager;

    [SerializeField]
    Button _acceptButton;

    [SerializeField]
    private Text _currentTrainingText;

    [SerializeField]
    private Text _realGoodLearninText;

    [SerializeField]
    private Text _goodLearninText;

    [SerializeField]
    private Text _badLearninText;

    // Use this for initialization
    void Start()
	{
        // Grab the statistics manager
        _statManager = GameObject.Find( "GameInfo" ).GetComponent<StatManager>();

        // Erase the text in the current training
        _currentTrainingText.text = "None";
        _realGoodLearninText.text = "";
        _goodLearninText.text = "";
        _badLearninText.text = "";
    }
	
	// Update is called once per frame
	void Update()
	{
	
	}

    public void SetNextTrainingToApply( Training value )
    {
        _statManager.SetNextTrainingToApply( value );
        _currentTrainingText.text = value.Title;
        _realGoodLearninText.text = value.GetRealGoodLearnin().ToString();
        _goodLearninText.text = value.GetGoodLearnin().ToString();
        _badLearninText.text = value.GetBadLearnin().ToString();

        // Enable the button when training is selected
        _acceptButton.interactable = true;
    }

    public void ApplyNextTraining()
    {
        _statManager.ApplyNextTraining();

        // Reset text
        _currentTrainingText.text = "None";
        _realGoodLearninText.text = "";
        _goodLearninText.text = "";
        _badLearninText.text = "";
    }
}
