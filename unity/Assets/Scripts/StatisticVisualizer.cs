﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatisticVisualizer : MonoBehaviour
{
    [SerializeField]
    private Text _title;

    [SerializeField]
    private GameObject _fill;
    private RectTransform _fillRectTransform;

    [SerializeField]
    private GameObject _cursor;
    private RectTransform _cursorRectTransform;
    private Image _cursorImage;
    
    private StatManager _statManager;

    private float _cursorTotalDistance;
    private float _cursorOriginalXPosition;
    
    // Use Awake here because Start occurs after OnEnable()
    void Awake()
	{
        // Grab the statistics manager
        _statManager = GameObject.Find( "GameInfo" ).GetComponent<StatManager>();

        // Get the rect transforms
        _fillRectTransform = _fill.GetComponent<RectTransform>();
        _cursorRectTransform = _cursor.GetComponent<RectTransform>();
        _cursorImage = _cursor.GetComponent<Image>();

        // Get the initial width of the fill rectangle before we scale it so that we know how far to move the cursor.
        // The MainScreen object has been scaled down so we need to get that scale factor too.  We can use the MainScreen's
        // localToWorld transform and grab the x-value of the scale since no rotations have been applied.  We don't want
        // to transform the point by the matrix since we just need the scale.
        _cursorTotalDistance = _fillRectTransform.rect.width * GameObject.Find( "MainScreen" ).GetComponent<RectTransform>().localToWorldMatrix.m00;
        
        // Get the initial x-position of the cursor before we move it so that we know how far to adjust the cursor
        _cursorOriginalXPosition = _cursorRectTransform.position.x;

        // Set the title text to the object name
        _title.text = this.name;
    }
    
    // Update is called once per frame
    void Update()
	{
    }
    
    void OnEnable()
    {
        // We need null checks here because this can be called during Unity serialization/deserialization when these values won't be set
        // since the stat manager won't exist
        if( ( null != _statManager ) && ( null != _fillRectTransform ) && ( null != _cursorRectTransform ) )
        {
            // Get the statistic represented as a float
            float stat = _statManager.GetStatValue( this.name );

            // Find which version of the cursor we want to use based on stat level
            string spriteToLoad = "0";
            if( 10.0f == stat )
            {
                spriteToLoad = "10";
            }
            else if( stat >= 7.0f )
            {
                spriteToLoad = "7-9";
            }
            else if( stat >= 4.0f )
            {
                spriteToLoad = "4-6";
            }
            else if( stat >= 1.0f )
            {
                spriteToLoad = "1-3";
            }
            
            // Replace the sprite with the appropriate image
            _cursorImage.sprite = Resources.Load( "Images/" + spriteToLoad, typeof( Sprite ) ) as Sprite;

            // Get the statistic represented as a percentage
            float statPercent = _statManager.GetStatPercent( this.name );
            
            // Scale the fill in the x-direction based on that percentage - the pivot is on the left side
            _fillRectTransform.localScale = new Vector3( statPercent, 1.0f, 1.0f );
            
            // Move the cursor in the x-direction using the parametric equation:  start + ( percent * totalLength )
            _cursorRectTransform.position = new Vector3( _cursorOriginalXPosition + ( statPercent * _cursorTotalDistance ), _cursorRectTransform.position.y, _cursorRectTransform.position.z );
        }
    }
}
