﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MainScreenUIManager : MonoBehaviour
{
    [SerializeField]
    private Text _weekText;

    [SerializeField]
    private GameObject _strike1;

    [SerializeField]
    private GameObject _strike2;

    [SerializeField]
    private GameObject _strike3;

    [SerializeField]
    private Button _acceptButton;

    [SerializeField]
    private GameObject _helpScreen;

    // The grandpa actor in the scene
    private Actor _grandpa;
    // A reference to the renderer so we can change outfits
    private SpriteRenderer _grandpaSpriteRenderer;

    private SceneLoader _sceneLoader;
    private GameInfo _gameInfo;
    private StatManager _statManager;
    private ActorManager _actorManager;

    void Start()
    {
        // Get some game components
        GameObject gameInfoGameObject = GameObject.Find( "GameInfo" );
        _sceneLoader = gameInfoGameObject.GetComponent<SceneLoader>();
        _gameInfo = gameInfoGameObject.GetComponent<GameInfo>();
        _statManager = gameInfoGameObject.GetComponent<StatManager>();
        _actorManager = gameInfoGameObject.GetComponent<ActorManager>();

        // If we have never showed the help screen, show it
        if( !_gameInfo.ShowedHelpScreen )
        {
            _gameInfo.ShowedHelpScreen = true;
            _helpScreen.SetActive( true );
        }

        // Initialize the grandpa actor
        _grandpa = Instantiate( _actorManager.GetActor( "Grandpa" ), new Vector3( 0.0f, 0.0f, 0.0f ), Quaternion.identity ) as Actor;
        // Get the sprite renderer.  We know it's on the MainSprite object which is the only chlid of the actor
        _grandpaSpriteRenderer = _grandpa.transform.GetChild( 0 ).GetComponent<SpriteRenderer>();

        // Disable the button until training is selected
        _acceptButton.interactable = false;

        // Update the current week and set the text
        _gameInfo.IncrementWeek();
        UpdateWeek();

        // If this is the first week, select the plain outfit.
        // Even though this is called by the StartMenu script when the play button is pressed, we need to trigger it here so that it upates
        // the actor in this scene
        if( 1 == _gameInfo.Week )
        {
            OnOutfitSelectedInternal( "grandpa_plain" );
        }

        // Update the strikes
        UpdateStrikes();
        
    }

    public void UpdateWeek()
    {
        _weekText.text = _gameInfo.Week.ToString();
    }

    public void UpdateStrikes()
    {
        // Hide the inactive strikes
        _strike1.SetActive( _gameInfo.Strikes > 0 );
        _strike2.SetActive( _gameInfo.Strikes > 1 );
        _strike3.SetActive( _gameInfo.Strikes > 2 );
    }

    // Update is called once per frame
    void Update()
	{
	
	}

    public void OnQuitPressed()
    {
        _sceneLoader.LoadQuitMenu();
    }

    public void EnableStrike1()
    {
        _strike1.SetActive( true );
    }

    public void EnableStrike2()
    {
        _strike2.SetActive( true );
    }

    public void EnableStrike3()
    {
        _strike3.SetActive( true );
    }

    public void OnAcceptPressed()
    {
        // Force clothes for the week 12 test in order to match post-intro event
        if( 12 == _gameInfo.Week )
        {
            OnOutfitSelectedInternal( "grandpa_brownsuit" );
        }

        // Destroy the grandpa actor that we instantiated
        Destroy( _grandpa.gameObject );
        _grandpa = null;
        _grandpaSpriteRenderer = null;
        
        _sceneLoader.OnSceneEnded( SceneLoader.SceneEnum.MainScreen );
    }

    public void OnOutfitSelected( GameObject obj )
    {
        // We use a GameObject in the call from the scene to take advantage of triggers that persist through the prefab
        OnOutfitSelectedInternal( obj.name );
    }

    void OnOutfitSelectedInternal( string outfit )
    {
        // Change the outfit on the grandpa sprite on the main screen, and inform the Actor Manager
        _grandpaSpriteRenderer.sprite = Resources.Load( "Images/Characters/Grandpa/" + outfit, typeof( Sprite ) ) as Sprite;
        _actorManager.OnOutfitChanged( outfit );
    }
    
    public void RandomizePoiseStat()
    {
        _statManager.SetStatValue( "Poise", Random.Range( StatManager.MIN_STAT_VALUE, StatManager.MAX_STAT_VALUE ) );
    }
}
