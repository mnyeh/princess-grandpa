﻿using UnityEngine;
using System.Collections;

public class URLLoader : MonoBehaviour
{
    [SerializeField]
    private string _url;

	// Use this for initialization
	void Start()
	{
	
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}

    public void OnURLClicked()
    {
        Application.OpenURL( _url );
    }
}
