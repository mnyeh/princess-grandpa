﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

/** This class plays the intro movie, and on input, moves to the menu scene */
public class IntroMoviePlayer : MonoBehaviour
{

    [SerializeField]
    private MovieTexture _videoChannel;

    private AudioSource _audioChannel;
    private SceneLoader _sceneLoader;
    
    void Start()
    {
        _sceneLoader = GameObject.Find( "GameInfo" ).GetComponent<SceneLoader>();
        
        // Play the video
        _videoChannel.Play();
        // Grab the audio source applied in the editor and store it off, and play the audio
        _audioChannel = GetComponent<AudioSource>();
        _audioChannel.Play();
    }

    void Update()
    {
        // If any buttons are pressed, stop the video
        if( Input.anyKeyDown )
        {
            _videoChannel.Stop();
            _audioChannel.Stop();
        }

        // If the video is done playing, call the end scene delegate
        if( !_videoChannel.isPlaying )
        {
            _sceneLoader.OnSceneEnded( SceneLoader.SceneEnum.Intro );
        }
    }

    void OnGUI()
    {
        // Draw the actual video
        GUI.DrawTexture( new Rect( 0.0f, 0.0f, Screen.width, Screen.height ), _videoChannel );
    }
}
