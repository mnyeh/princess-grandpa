﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    /** This enum needs to match the scene order in the build settings */
    public enum SceneEnum
    {
        Game = 0,
        Intro,
        StartMenu,
        MainScreen,
        QuitMenu,
        Event,
        Gameover
    };

    [SerializeField]
    private SceneEnum _sceneToLoad = SceneEnum.Intro;

    private SceneEnum _currentScene = SceneEnum.Game;

    private GameInfo _gameInfo;
    private StatManager _statManager;
    private ActorManager _actorManager;

    void UnloadCurrentScene()
    {
        // Never unload the game scene
        if( SceneEnum.Game != _currentScene)
        {
            SceneManager.UnloadScene( (int) _currentScene );
        }
    }

    private void LoadScene( SceneEnum scene )
    {
        // Can't load the Game scene - it should always be loaded
        if( SceneEnum.Game != scene )
        {
            UnloadCurrentScene();

            // Load the scene and store it as the current scene
            SceneManager.LoadScene( (int) scene, LoadSceneMode.Additive );
            _currentScene = scene;
        }
    }

    /** This is a special case to load the quit menu without unloading the current scene first */
    public void LoadQuitMenu()
    {
        SceneManager.LoadScene( (int) SceneEnum.QuitMenu, LoadSceneMode.Additive );
    }

    /** This is a special case to unload the quit menu */
    public void UnloadQuitMenu()
    {
        SceneManager.UnloadScene( (int) SceneEnum.QuitMenu );
    }

    // Use this for initialization
    void Start()
    {
        _gameInfo = GetComponent<GameInfo>();
        _statManager = GetComponent<StatManager>();
        _actorManager = GetComponent<ActorManager>();

        // Initialize the intro scene
        LoadScene( _sceneToLoad );
	}
	
	// Update is called once per frame
	void Update()
    {
	
	}

    public string GetCurrentXMLFile()
    {
        string ret = "";

        // If the week is -1, it means we never initialized game vars - we're still pre-start menu, so play the post-intro scene
        if( _gameInfo.Week < 0 )
        {
            ret = "PostIntro";
        }
        else if( _gameInfo.ShouldTransitionToEndingScene() )
        {
            if( _gameInfo.StruckOut )
            {
                ret = "Gameover";
            }
            else if( _gameInfo.NopeTestDone )
            {
                ret = _gameInfo.EndingEventFilename;
            }
        }
        else
        {
            ret = "Event" + _gameInfo.Week;

            // If we're going into the 7th event, see if we have Bruno as a friend to trigger a different path
            if( ( 7 == _gameInfo.Week ) && ( 1.0f == _statManager.GetStatValue( "Bruno" ) ) )
            {
                ret += "bruno";
            }

            // If were' going into events 1-11, see if we are nude to trigger a different path
            if( ( ( 0 < _gameInfo.Week ) && ( _gameInfo.Week < 12 ) ) && _actorManager.GrandpaNude )
            {
                ret += "nude";
            }
        }

        return ret;
    }

    public void OnSceneEnded( SceneEnum scene )
    {
        switch( scene )
        {
            case SceneEnum.Gameover: // On Gameover, go back to the start menu
                LoadScene( SceneEnum.StartMenu );
                break;
            case SceneEnum.MainScreen:
            case SceneEnum.Intro: // Go to the post-intro event scene
            case SceneEnum.StartMenu: // StartMenu plays Event0 first
                LoadScene( SceneEnum.Event );
                break;
            case SceneEnum.Event:
                // If the Bruno sequence just ended, see if we have gained Bruno as a friend
                if( 3 == _gameInfo.Week )
                {
                    if( ( _statManager.GetStatValue( "Tolerance" ) >= 2 ) &&
                        ( _statManager.GetStatValue( "Knowledge" ) >= 2 ) &&
                        ( _statManager.GetStatValue( "Coolness" ) >= 2 ) )
                    {
                        _statManager.SetStatValue( "Bruno", 1.0f );
                    }
                }

                // We need to see if we lost Bruno as a friend by sending him a dickpic or failed emoji
                // This can happen on bruno or brunonude, but the stat check is the same either way, so we only
                // need to check for the bruno flag
                if( ( 7 == _gameInfo.Week ) && ( 1.0f == _statManager.GetStatValue( "Bruno" ) ) )
                {
                    if( _statManager.GetStatValue( "Communication" ) < 5 )
                    {
                        _statManager.SetStatValue( "Bruno", 0.0f );
                    }
                }

                LoadScene( _gameInfo.ShouldTransitionToEndingScene() ? SceneEnum.Event : SceneEnum.MainScreen );
                break;
        }
    }
}
