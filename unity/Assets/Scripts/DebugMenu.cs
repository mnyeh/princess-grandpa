﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class DebugMenu : MonoBehaviour
{
    [SerializeField]
    GameObject _debugButton;
    [SerializeField]
    InputField _weekInputField;
    [SerializeField]
    InputField _strikesInputField;
    [SerializeField]
    InputField _toleranceInputField;
    [SerializeField]
    InputField _communicationInputField;
    [SerializeField]
    InputField _technologyInputField;
    [SerializeField]
    InputField _trendsInputField;
    [SerializeField]
    InputField _grandpaInputField;
    [SerializeField]
    InputField _knowledgeInputField;
    [SerializeField]
    InputField _nostalgiaInputField;
    [SerializeField]
    InputField _skillsInputField;
    [SerializeField]
    InputField _coolnessInputField;
    [SerializeField]
    InputField _brunoInputField;

    private GameInfo _gameInfo;
    private StatManager _statManager;

	// Use this for initialization
	void Start()
	{
        GameObject gameInfoGameObject = GameObject.Find( "GameInfo" );
        _gameInfo = gameInfoGameObject.GetComponent<GameInfo>();
        _statManager = gameInfoGameObject.GetComponent<StatManager>();
	}

    void OnGUI()
    {
        Event e = Event.current;
        if( e.isKey && e.control && e.keyCode == KeyCode.D )
        {
            _debugButton.SetActive( !_debugButton.activeSelf );
        }
    }
	
	// Update is called once per frame
	void Update()
	{
    }

    public void OnCloseButtonPressed()
    {
        MainScreenUIManager mainScreenUIManager = GetComponent<MainScreenUIManager>();

        if( "" != _weekInputField.text )
        {
            _gameInfo.Week = Int32.Parse( _weekInputField.text );
        }

        if( "" != _strikesInputField.text )
        {
            _gameInfo.Strikes = Int32.Parse( _strikesInputField.text );
        }

        if( "" != _toleranceInputField.text )
        {
            _statManager.SetStatValue( "Tolerance", float.Parse( _toleranceInputField.text ) );
        }

        if( "" != _communicationInputField.text )
        {
            _statManager.SetStatValue( "Communication", float.Parse( _communicationInputField.text ) );
        }

        if( "" != _technologyInputField.text )
        {
            _statManager.SetStatValue( "Technology", float.Parse( _technologyInputField.text ) );
        }

        if( "" != _trendsInputField.text )
        {
            _statManager.SetStatValue( "Trends", float.Parse( _trendsInputField.text ) );
        }

        if( "" != _grandpaInputField.text )
        {
            _statManager.SetStatValue( "Grandpa", float.Parse( _grandpaInputField.text ) );
        }

        if( "" != _knowledgeInputField.text )
        {
            _statManager.SetStatValue( "Knowledge", float.Parse( _knowledgeInputField.text ) );
        }

        if( "" != _nostalgiaInputField.text )
        {
            _statManager.SetStatValue( "Nostalgia", float.Parse( _nostalgiaInputField.text ) );
        }

        if( "" != _skillsInputField.text )
        {
            _statManager.SetStatValue( "Skills", float.Parse( _skillsInputField.text ) );
        }

        if( "" != _coolnessInputField.text )
        {
            _statManager.SetStatValue( "Coolness", float.Parse( _coolnessInputField.text ) );
        }

        if( "" != _brunoInputField.text )
        {
            _statManager.SetStatValue( "Bruno", float.Parse( _brunoInputField.text ) );
        }

        mainScreenUIManager.UpdateStrikes();
        mainScreenUIManager.UpdateWeek();
    }
}
