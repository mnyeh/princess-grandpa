﻿using UnityEngine;
using System.Collections;

public class QuitMenu : MonoBehaviour
{
    private SceneLoader _sceneLoader;

    public void OnCancelPressed()
    {
        _sceneLoader.UnloadQuitMenu();
    }

    public void OnQuitPressed()
    {
        Application.Quit();
    }

    // Use this for initialization
    void Start()
	{
        _sceneLoader = GameObject.Find( "GameInfo" ).GetComponent<SceneLoader>();
    }
	
	// Update is called once per frame
	void Update()
	{
	
	}
}
