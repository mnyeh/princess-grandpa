﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/** This class manages the stats within the game */
public class StatManager : MonoBehaviour
{
    /** Class describing statistics */
    private class Statistic
    {
        private float _stat;
        public float Stat { get { return _stat; } }
        
        /** Sets the initial stat value */
        public void SetInitialStatValue( float initialValue )
        {
            _stat = initialValue;
        }

        /** Sets the statistic to the amount and clamps it */
        public void SetStat( float amount )
        {
            // Clamp the new value by the min and max values
            if( amount < MIN_STAT_VALUE )
            {
                amount = MIN_STAT_VALUE;
            }
            else if( amount > MAX_STAT_VALUE )
            {
                amount = MAX_STAT_VALUE;
            }

            _stat = amount;
        }
    }
    
    /** Enumeration for all of the statistics */
    public enum StatEnum {  Tolerance, Communication, Technology, Trends, Grandpa, Knowledge, Nostalgia, Skills, Coolness };

    /** Initial stat values */
    [Header("Initial Values")]
    [SerializeField]
    private float _initialToleranceValue;
    [SerializeField]
    private float _initialCommunicationValue;
    [SerializeField]
    private float _initialTechnologyValue;
    [SerializeField]
    private float _initialTrendsValue;
    [SerializeField]
    private float _initialGrandpaValue;
    [SerializeField]
    private float _initialKnowledgeValue;
    [SerializeField]
    private float _initialNostalgiaValue;
    [SerializeField]
    private float _initialSkillsValue;
    [SerializeField]
    private float _initialCoolnessValue;
    [SerializeField]
    private float _initialPoiseValue;

    /** Final stat values for NOPE test to evaluate against */
    [Header("Final Values")]
    [SerializeField]
    private float _finalToleranceValue;
    public float FinalToleranceValue { get { return _finalToleranceValue; } }
    [SerializeField]
    private float _finalCommunicationValue;
    public float FinalCommunicationValue { get { return _finalCommunicationValue; } }
    [SerializeField]
    private float _finalTechnologyValue;
    public float FinalTechnologyValue { get { return _finalTechnologyValue; } }
    [SerializeField]
    private float _finalTrendsValue;
    public float FinalTrendsValue { get { return _finalTrendsValue; } }
    [SerializeField]
    private float _finalGrandpaValue;
    public float FinalGrandpaValue { get { return _finalGrandpaValue; } }
    [SerializeField]
    private float _finalKnowledgeValue;
    public float FinalKnowledgeValue { get { return _finalKnowledgeValue; } }
    [SerializeField]
    private float _finalNostalgiaValue;
    public float FinalNostalgiaValue { get { return _finalNostalgiaValue; } }
    [SerializeField]
    private float _finalSkillsValue;
    public float FinalSkillsValue { get { return _finalSkillsValue; } }
    [SerializeField]
    private float _finalCoolnessValue;
    public float FinalCoolnessValue { get { return _finalCoolnessValue; } }
    [SerializeField]
    private float _finalPoiseValue;
    public float FinalPoiseValue { get { return _finalPoiseValue; } }

    /** Dictionary of stat names -> statistics */
    private Dictionary< string, Statistic > _statsDict = new Dictionary<string, Statistic>();

    /** Constant variables governing statistics */
    public static readonly float MIN_STAT_VALUE = 0.0f;
    public static readonly float MAX_STAT_VALUE = 10.0f;
    private static readonly float REAL_GOOD_LEARNIN_STEP = 2.0f;
    private static readonly float GOOD_LEARNIN_STEP = 1.0f;
    private static readonly float BAD_LEARNIN_STEP = -1.0f;

    private Training _nextTrainingToApply;

    // Use this for initialization
    void Start()
    {
        // Initialize dictionary entries
        _statsDict.Add( "Tolerance", new Statistic() );
        _statsDict.Add( "Communication", new Statistic() );
        _statsDict.Add( "Technology", new Statistic() );
        _statsDict.Add( "Trends", new Statistic() );
        _statsDict.Add( "Grandpa", new Statistic() );
        _statsDict.Add( "Knowledge", new Statistic() );
        _statsDict.Add( "Nostalgia", new Statistic() );
        _statsDict.Add( "Skills", new Statistic() );
        _statsDict.Add( "Coolness", new Statistic() );
        _statsDict.Add( "Poise", new Statistic() );

        // Hidden Bruno Mars stat
        _statsDict.Add( "Bruno", new Statistic() );
    }

    public void InitializeStats()
    {
        _statsDict[ "Tolerance" ].SetInitialStatValue( _initialToleranceValue);
        _statsDict[ "Communication" ].SetInitialStatValue( _initialCommunicationValue);
        _statsDict[ "Technology" ].SetInitialStatValue( _initialTechnologyValue);
        _statsDict[ "Trends" ].SetInitialStatValue( _initialTrendsValue);
        _statsDict[ "Grandpa" ].SetInitialStatValue( _initialGrandpaValue);
        _statsDict[ "Knowledge" ].SetInitialStatValue( _initialKnowledgeValue);
        _statsDict[ "Nostalgia" ].SetInitialStatValue( _initialNostalgiaValue);
        _statsDict[ "Skills" ].SetInitialStatValue( _initialSkillsValue);
        _statsDict[ "Coolness" ].SetInitialStatValue( _initialCoolnessValue);
        _statsDict[ "Poise" ].SetInitialStatValue( _initialPoiseValue);
        _statsDict[ "Bruno" ].SetInitialStatValue( 0.0f );
    }

    // Update is called once per frame
    void Update()
    {

    }

    /** Returns the value of a stat */
    public float GetStatValue( string statName )
    {
        return _statsDict[ statName ].Stat;
    }

    /** Sets the value of a stat */
    public void SetStatValue( string statName, float value )
    {
        _statsDict[ statName ].SetStat( value );
    }

    /** Returns the value of a stat as a percentage of the stat range */
    public float GetStatPercent( string statName )
    {
        return GetStatValue( statName ) / ( MAX_STAT_VALUE - MIN_STAT_VALUE );
    }
    
    public void SetNextTrainingToApply( Training value )
    {
        _nextTrainingToApply = value;
    }

    public void ApplyNextTraining()
    {
        if( null != _nextTrainingToApply )
        {
            // Apply training
            Statistic realGoodStat = _statsDict[ _nextTrainingToApply.GetRealGoodLearnin().ToString() ];
            Statistic goodStat = _statsDict[ _nextTrainingToApply.GetGoodLearnin().ToString() ];
            Statistic badStat = _statsDict[ _nextTrainingToApply.GetBadLearnin().ToString() ];
            realGoodStat.SetStat( realGoodStat.Stat + REAL_GOOD_LEARNIN_STEP );
            goodStat.SetStat( goodStat.Stat + GOOD_LEARNIN_STEP );
            badStat.SetStat( badStat.Stat + BAD_LEARNIN_STEP );

            // Reset next training so it can't be spammed
            _nextTrainingToApply = null;
        }
    }
}
