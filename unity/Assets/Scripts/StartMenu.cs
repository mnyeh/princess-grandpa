﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartMenu : MonoBehaviour
{
    [SerializeField]
    private Button _playButton;

    [SerializeField]
    private Button _quitButton;

    private SceneLoader _sceneLoader;
    private StatManager _statManager;
    private ActorManager _actorManager;
    private GameInfo _gameInfo;

    // Use this for initialization
    void Start()
    {
        GameObject gameInfoGameObject =  GameObject.Find( "GameInfo" );
        _sceneLoader = gameInfoGameObject.GetComponent<SceneLoader>();
        _statManager = gameInfoGameObject.GetComponent<StatManager>();
        _actorManager = gameInfoGameObject.GetComponent<ActorManager>();
        _gameInfo = gameInfoGameObject.GetComponent<GameInfo>();
    }

    public void OnQuitPressed()
    {
        _sceneLoader.LoadQuitMenu();
    }

    public void OnPlayPressed()
    {
        // Reset stats to their initial values
        _statManager.InitializeStats();

        // Reset week and strikes
        _gameInfo.InitializeGameInfo();

        _sceneLoader.OnSceneEnded( SceneLoader.SceneEnum.StartMenu );

        // Reset to the "plain" outfit
        _actorManager.OnOutfitChanged( "grandpa_plain" );
    }
    
	// Update is called once per frame
	void Update()
    {
	
	}
}
