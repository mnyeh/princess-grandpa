﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class Actor : MonoBehaviour
{
    [SerializeField]
    private int _audioFrameDelay;
    public int AudioFrameDelay { get { return _audioFrameDelay; } }

    [SerializeField]
    private AudioClip _voice;
    public AudioClip Voice { get { return _voice; } }

    [SerializeField]
    private float _voiceLowPitch = 0.9f;
    public float VoiceLowPitch { get { return _voiceLowPitch; } }

    [SerializeField]
    private float _voiceHighPitch = 1.1f;
    public float VoiceHighPitch { get { return _voiceHighPitch; } }

    [SerializeField, Range( 0.0f, 1.0f )]
    private float _voiceVolume;
    public float Volume { get { return _voiceVolume; } }
    
    Action<Actor> _transitionOutCallback;
    public void setTransitionOutCallback( Action<Actor> callback )
    {
        _transitionOutCallback = callback;
    }

    public void OnTransitionOutFinished()
    {
        if( null != _transitionOutCallback )
        {
            _transitionOutCallback( this );
            _transitionOutCallback = null;
        }
    }
    
    // Use this for initialization
    void Start()
	{
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}
}
