﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameInfo : MonoBehaviour
{
    private StatManager _statManager;

    private int _week = -1; // Start at -1; start menu will set it to 0, main screen will increment it on Start()
    public int Week { get { return _week; } set { _week = value; } }

    private bool _showedHelpScreen = false;
    public bool ShowedHelpScreen { get { return _showedHelpScreen; } set { _showedHelpScreen = value; } }

    private bool _nopeTestDone = false;
    public bool NopeTestDone
    {
        get
        {
            return _nopeTestDone;
        }
        set
        {
            _nopeTestDone = value;

            // If we're finishing the nope test, figure out which ending we should transition to
            if( value )
            {
                float grandpaStatValue = _statManager.GetStatValue( "Grandpa" );
                if( _statManager.FinalGrandpaValue == grandpaStatValue )
                {
                    _endingEventFilename = "UrGrandpa";
                }
                else if( PassedNopeTest() )
                {
                    if( grandpaStatValue >= 7.0f ) // between 7 and 9, 10 is covered above
                    {
                        _endingEventFilename = "Subterfuge";
                    }
                    else
                    {
                        _endingEventFilename = "Subjugate";
                    }
                }
                else // Did not pass nope test
                {
                    if( 1.0f == _statManager.GetStatValue( "Bruno" ) ) // Can Bruno reedem grandpa
                    {
                        _endingEventFilename = "Bruno";
                    }
                    else
                    {
                        _endingEventFilename = "Sandbags";
                    }
                }
            }
        }
    }

    private string _endingEventFilename = "";
    public string EndingEventFilename { get { return _endingEventFilename; } }

    private int _strikes;
    public int Strikes { get { return _strikes; } set { _strikes = value; } }
    public bool StruckOut { get { return ( _strikes > 2 ); } }
    
	// Use this for initialization
	void Start()
	{
        _statManager = GetComponent<StatManager>();

    }

    public void InitializeGameInfo()
    {
        _week = 0;
        _strikes = 0;
        NopeTestDone = false;
        _endingEventFilename = "";
    }

    public bool ShouldTransitionToEndingScene()
    {
        return ( StruckOut || NopeTestDone );
    }
	
	// Update is called once per frame
	void Update()
	{
	
	}

    /** Increment the week by 1 */
    public void IncrementWeek()
    {
        _week++;
    }

    public void AddStrikes( int value )
    {
        _strikes += value;
        print( "Strikes:  " + _strikes );
    }

    bool PassedNopeTest()
    {
        int score = 0;
        if( _statManager.GetStatValue( "Tolerance") >= _statManager.FinalToleranceValue )
        {
            score++;
        }

        if( _statManager.GetStatValue( "Skills" ) >= _statManager.FinalSkillsValue )
        {
            score++;
        }

        if( _statManager.GetStatValue( "Trends" ) >= _statManager.FinalTrendsValue )
        {
            score++;
        }

        if( _statManager.GetStatValue( "Coolness" ) >= _statManager.FinalCoolnessValue )
        {
            score++;
        }

        if( _statManager.GetStatValue( "Technology" ) >= _statManager.FinalTechnologyValue )
        {
            score++;
        }

        if( _statManager.GetStatValue( "Communication" ) >= _statManager.FinalCommunicationValue )
        {
            score++;
        }

        if( _statManager.GetStatValue( "Knowledge" ) >= _statManager.FinalKnowledgeValue )
        {
            score++;
        }
        
        return ( score > 3 );
    }
}
