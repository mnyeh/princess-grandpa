﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Training : MonoBehaviour
{
    [SerializeField]
    private Text _title;
    public string Title { get { return _title.text; } }

    [SerializeField]
    private Text _realGoodLearninPreview;

    [SerializeField]
    private Text _goodLearninPreview;

    [SerializeField]
    private Text _badLearninPreview;

    [SerializeField]
    private StatManager.StatEnum _realGoodLearnin;
    public StatManager.StatEnum GetRealGoodLearnin()
    {
        return _realGoodLearnin;
    }

    [ SerializeField]
    private StatManager.StatEnum _goodLearnin;
    public StatManager.StatEnum GetGoodLearnin()
    {
        return _goodLearnin;
    }

    [SerializeField]
    private StatManager.StatEnum _badLearnin;
    public StatManager.StatEnum GetBadLearnin()
    {
        return _badLearnin;
    }

    private TrainingManager _trainingManager;

    // Use this for initialization
    void Start()
    {
        // Grab the training manager
        _trainingManager = GameObject.Find( "UIManager" ).GetComponent<TrainingManager>();

        // Set the title text to the object name
        _title.text = this.name;

        // Erase the text in the previews
        _realGoodLearninPreview.text = "";
        _goodLearninPreview.text = "";
        _badLearninPreview.text = "";
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnButtonClicked()
    {
        _trainingManager.SetNextTrainingToApply( this );
    }

    public void OnMouseEnterButton()
    {
        _realGoodLearninPreview.text = _realGoodLearnin.ToString();
        _goodLearninPreview.text = _goodLearnin.ToString();
        _badLearninPreview.text = _badLearnin.ToString();
    }

    public void OnMouseExitButton()
    {
        _realGoodLearninPreview.text = "";
        _goodLearninPreview.text = "";
        _badLearninPreview.text = "";
    }
}
