﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;

public class StatCheck : MonoBehaviour
{
    [SerializeField]
    private Text _statText;
    public Text StatText { get { return _statText; } }

    Action<StatCheck> _statCheckFinishedCallback;
    public void setStatCheckFinishedCallback( Action<StatCheck> callback )
    {
        _statCheckFinishedCallback = callback;
    }

    public void OnStatCheckFinished()
    {
        if( null != _statCheckFinishedCallback )
        {
            _statCheckFinishedCallback( this );
            _statCheckFinishedCallback = null;
        }
    }

    // Use this for initialization
    void Start()
	{
	
	}
	
	// Update is called once per frame
	void Update()
	{
	
	}
}
